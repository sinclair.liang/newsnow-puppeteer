# newsnow-puppeteer

## How to run

```
npm i
```

```
npm run newsnow
```

## CloudWatch

### Environment Variable used

`TESTCASETITLE`: the header of the dimension of every test case

`CFTIMETITLE`: the header of the metric title of color factory load time

`CFDIMENSIONTITLE`: the header of the dimension of color factory load time

`CFDIMENSIONVALUE`: the value of the dimension of color factory load time