import {sendExecuted, sendResult, updateLoadTime} from './functions/uploadToCloudWatch';
import mocha from 'mocha'
import Slack from 'node-slack'
import fs from 'fs'
import path from 'path'

const hookUrl = "https://hooks.slack.com/services/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"
const slack = new Slack(hookUrl);

module.exports = NewsNowReporter;

function NewsNowReporter(runner) {
  mocha.reporters.Base.call(this, runner);
  let passes = 0;
  let failures = 0;

  runner.on('test', function(test) {
    sendExecuted(test.fullTitle())
  });

  runner.on('pass', function(test) {
    let passMessage = `Pass: ${test.fullTitle()}`
    console.log(passMessage)

    passes++;
    sendResult(test.fullTitle(), true)
  });

  runner.on('fail', function(test, err) {
    let failMessage
    failures++;
    if (fs.existsSync(path.join(__dirname, './results/testPage.json'))) {
        let testPageData = fs.readFileSync(path.join(__dirname, './results/testPage.json'))
        let testPage = JSON.parse(testPageData);

        if(!!err.message){
          failMessage = `Failed on ${testPage.pageUrl}\n${test.fullTitle().toUpperCase()}\n- ${err.message}`
        } else {
          failMessage = `Failed on ${testPage.pageUrl}\n${test.fullTitle().toUpperCase()}\n`
        }
    } else {
        failMessage = `Failed: ${test.fullTitle().toUpperCase()}\n- ${err.message.match(/expected.*/)[0]}`
    }
    
    console.log(failMessage)
    slack.send({
        text: failMessage,
        icon_emoji: 'exclamation',
        username: 'Newsnow Bot'
    });
    sendResult(test.fullTitle(), false)
  });

  runner.on('end', function() {
    if (fs.existsSync(path.join(__dirname, '../results/copyFitTime.json'))) {
      let copyFitTimeJSON = fs.readFileSync(path.join(__dirname, './results/copyFitTime.json'))
      let copyFitTime = JSON.parse(copyFitTimeJSON);
      updateLoadTime(copyFitTime)
    }
    console.log('Test finished: %d/%d passed', passes, passes + failures);
    // updateResult(passes / 6)
  });
}