
import Login from '../functions/Login.js';
import SideNav from '../functions/SideNav.js';
import InfoPanel from '../functions/InfoPanel.js';
import launchPuppeteer from '../functions/launchPuppeteer';
import fs from 'fs'
import path from 'path'


const monzaUser = process.env.MONZA_TEST_EMAIL
const monzaPassword = process.env.MONZA_TEST_PASSWORD
let baseUrl = "https://newsnow.io/"

describe('newsnow', function(){
    this.timeout(0);
    let browser
    let page
    let login
    let sideNav
    let infoPanel
    


    before (async () => {
        this.furnitureList = []
        this.storyList = []

        browser = await launchPuppeteer();
        page = await browser.newPage();
        await page.setViewport({ width: 1440, height: 800});
        login = await new Login(page);
        sideNav = await new SideNav(page);
        infoPanel = await new InfoPanel(page);
        if (fs.existsSync(path.join(__dirname, '../results/copyFitTime.json'))) {
            fs.unlinkSync(path.join(__dirname, '../results/copyFitTime.json'))
        }
    });
    
    after (async () => {
        await browser.close()
    })

    it('should log user in', async () => {
        await page.goto(baseUrl)
        await login.login(monzaUser, monzaPassword)
    })

    it('should open the book', async () => {
        await sideNav.openBook('AUTO', "2025-12-31")
        // https://newsnow.io/pages/AUTO/2025-12-31
    })

    it('should open a random page in the book', async () => {
        let pageNumber = await sideNav.openRandomPage()
    })

    it('should assign furnitures correctly', async () => {
        this.furnitureList = await infoPanel.assignFurnitures()
    });

    it('should assign stories correctly', async () => {
        this.storyList = await infoPanel.assignStories()
        if (this.storyList.length === 0) {
            console.log('Page does not have story hole')
        }
    });

    it('should validate elements in pdf', async() => {
        let pdfData = await infoPanel.crawlPdf()
        await infoPanel.validateElements(this.furnitureList, this.storyList, pdfData)
    });
});
