import puppeteer from 'puppeteer';

const defaultOptions = {
    headless: true,
    // defaultViewport: null,
    args:['--window-size=1600,900'],
    // slowMo: 100,
    devtools: false
    // slowMo: true
};

export default async (options = undefined) => {
    const puppeterOptions = (options === undefined) ? defaultOptions : options;
    return await puppeteer.launch(puppeterOptions);
};
