const sideNav = require('../pageObjects/sideNav.json')
const infoPanel = require('../pageObjects/infoPanel.json')
import fs from 'fs'
import path from 'path'
import moment from 'moment'

export default class SideNav {
    constructor(page) {
        this.page = page;
    }

    openBook = async (site, date = "2020-03-01") => {
        await this.page.waitForSelector(sideNav.pageButton)
        await this.page.click(sideNav.pageButton)
        await this.page.waitFor(100)

        await this.page.waitForSelector(sideNav.publicationSelect, {visible: true})
        await this.page.click(sideNav.publicationSelect)

        await this.page.waitForSelector(sideNav.publicationSearchBox, {visible: true})
        await this.page.type(sideNav.publicationSearchBox, site, {delay: 1})
        await this.page.keyboard.press('Enter')
        await this.page.waitFor(100)

        let myDay = date.split('-')[2]
        let month = date.split('-')[1]
        let year = date.split('-')[0]
        let flag = 0;
        while(flag !== 1){
            let monthYearElement = await this.page.$(sideNav.monthYear)
            let monthYearTextOnPage = await this.page.evaluate(el => el.innerText, monthYearElement);
            let monthYearOnPage = moment(monthYearTextOnPage, 'MMMM YYYY')
            let monthYearDateOnPage = monthYearOnPage.toDate()
            
            let monthYearChosen = moment(month + '/' + year, 'MM/YYYY')
            let monthYearDateChosen = monthYearChosen.toDate()

            if(monthYearOnPage.isBefore(monthYearChosen) === true){
                await this.page.click(sideNav.nextMonthButton)
            } else if(monthYearOnPage.isAfter(monthYearChosen) === true){
                await this.page.click(sideNav.prevMonthButton)
            } else {
                flag = 1
            }
        }
        await this.page.waitFor(300)
        let days = await this.page.$$('tr td')
        for (const day of days) {
            const dayText = await this.page.evaluate(el => el.innerText, day);
            if(dayText === myDay){
                const dayClassName = await day.getProperty('className')
                let dayClass = await dayClassName.jsonValue()
                if(!dayClass.includes('other-month')){
                    await day.click()
                    break;
                }
            }
        }
        await this.page.waitForSelector(sideNav.pageListContainer)
    }

    openRandomPage = async () => {
        let pageList = await this.page.$$(sideNav.pageList)
        let flag = 0
        for (let index = 0; index < pageList.length; index++) {
            const pageEl = pageList[index];
            const pageNumberEl = await pageEl.$('small')
            let pageNumber = await this.page.evaluate(el => el.innerText, pageNumberEl);
            pageNumber = pageNumber === 'Cover' ? 1 : pageNumber
            pageNumber = parseInt(pageNumber, 10)
            if (pageNumber > flag) {
                flag = pageNumber
            } else {
                pageList = pageList.slice(0, index)
                break;
            }
        }
        let randomPageNumber = Math.floor(Math.random() * Math.floor(pageList.length)) - 1
        randomPageNumber = randomPageNumber < 0 ? 0 : randomPageNumber
        const randomPageEl = pageList[randomPageNumber]
        // console.log(`\n\tTest Page: ${randomPageNumber + 1}\n`)

        

        // await this.page.evaluate((randomPageEl) => {
        //     const randomPage = randomPageEl
        //     randomPage.scrollTop = randomPage.offsetHeight;
        // }, randomPageEl);
        await randomPageEl.click()
        await this.page.waitForSelector(infoPanel.infoPanelContainer)
        await this.page.waitFor(200)

        let pageUrl = await this.page.url()
        let testPage = {
            "pageNumber": randomPageNumber + 1,
            pageUrl
        }
        let testPageJSON = JSON.stringify(testPage)
        let filePath = path.resolve(__dirname, `../results/testPage.json`)
        fs.writeFile(filePath, testPageJSON, (err) => {if(err) throw err;})

        return randomPageNumber + 1

        // await this.page.focus(scrollable_section);
    }
}