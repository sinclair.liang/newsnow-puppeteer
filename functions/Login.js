const loginPage = require('../pageObjects/login.json')

export default class Login {

    constructor(page) {
        this.page = page;
    }

    login = async (email, password) => {
        await this.page.waitForSelector(loginPage.emailField)
        await this.page.type(loginPage.emailField, email, {delay: 1})
        await this.page.type(loginPage.passwordField, password, {delay: 1})

        await this.page.waitForSelector(loginPage.loginButton)
        await this.page.click(loginPage.loginButton)
        await this.page.waitFor(2000)
    }
}