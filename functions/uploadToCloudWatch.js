let AWS = require("aws-sdk");
AWS.config.update({ region: "ap-southeast-2" });
let testcasesTitle = process.env.TESTCASETITLE
let copyFitLoadTimeTitle = process.env.CFTIMETITLE
let copyFitDimensionTitle = process.env.CFDIMENSIONTITLE
let copyFitDimensionValue = process.env.CFDIMENSIONVALUE

// Create parameters JSON for putMetricData
module.exports = {sendExecuted, sendResult, updateLoadTime};

function sendExecuted(title){
    // Create CloudWatch service object
    var cw = new AWS.CloudWatch({ apiVersion: "2010-08-01" });

    let params = {
        MetricData: [
          {
            MetricName: "Executed",
            Dimensions: [
              {
                Name: testcasesTitle,
                Value: title
              }
            ],
            Unit: "Count",
            Value: 1,
            Timestamp: new Date()
          },
        ],  
        Namespace: "Newsnow/Healthchecks"
      };
      
      cw.putMetricData(params, function(err, data) {
        if (err) {
          console.log("Error", err);
        } else {
          console.log("Success", JSON.stringify(data));
        }
      });
}

function sendResult(title, pass){
    let passCount = pass ? 1 : 0
    let failCount = 1 - passCount
    // Create CloudWatch service object
    var cw = new AWS.CloudWatch({ apiVersion: "2010-08-01" });

    let params = {
        MetricData: [
          {
            MetricName: "Pass",
            Dimensions: [
              {
                Name: testcasesTitle,
                Value: title
              }
            ],
            Unit: "Count",
            Value: passCount,
            Timestamp: new Date()
          },
          {
            MetricName: "Fail",
            Dimensions: [
              {
                Name: testcasesTitle,
                Value: title
              }
            ],
            Unit: "Count",
            Value: failCount,
            Timestamp: new Date()
          },
        ],  
        Namespace: "Newsnow/Healthchecks"
      };
      
      cw.putMetricData(params, function(err, data) {
        if (err) {
          console.log("Error", err);
        } else {
          console.log("Success", JSON.stringify(data));
        }
      });
}


function updateLoadTime(loadTime){
    // Create CloudWatch service object
    var cw = new AWS.CloudWatch({ apiVersion: "2010-08-01" });

    let params = {
        MetricData: [
          {
            MetricName: copyFitLoadTimeTitle,
            Dimensions: [
              {
                Name: copyFitDimensionTitle,
                Value: copyFitDimensionValue
              }
            ],
            Unit: "Seconds",
            Value: loadTime,
            Timestamp: new Date()
          }
        ],
        Namespace: "Newsnow/CopyFitLoadtime"
      };
      
      cw.putMetricData(params, function(err, data) {
        if (err) {
          console.log("Error", err);
        } else {
          console.log("Success", JSON.stringify(data));
        }
      });
}