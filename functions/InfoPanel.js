const infoPanel = require('../pageObjects/infoPanel.json')
import {expect} from 'chai'
// import crawler from 'crawler-request'
const crawler = require('crawler-request')
import fs from 'fs'
import path from 'path'

export default class InfoPanel {
    constructor(page) {
        this.page = page;
    }

    getMedia = async () => {
        // await this.page.waitFor(3000)

        let mediaList = await this.page.$$(infoPanel.mediaList)

        let mediaResult = {
            "furnitureList":[],
            "storyList":[]
        }
        for (const media of mediaList) {
            const mediaTitle = await this.page.evaluate(el => el.innerText, media);
            if(mediaTitle.includes('Furniture')){
                let mediaNumber = mediaTitle.split(' ')[1]
                let _media = {
                    "number": mediaNumber,
                    "xSelector": `//span[contains(., 'Furniture ${mediaNumber}')]`
                }
                mediaResult.furnitureList.push(_media)
            } else if(mediaTitle.includes('Stories')){
                let mediaNumber = mediaTitle.split(' ')[1]
                let _media = {
                    "number": mediaNumber,
                    "xSelector": `//span[contains(., 'Stories ${mediaNumber}')]`
                }
                mediaResult.storyList.push(_media)
            }
        }

        return mediaResult
    }

    assignFurnitures = async () => {
        let existingFurniture = []
        let copyFitTimeList = []
        for (let index = 1; index < 5; index++) {
            let furnitureSize = await this.page.evaluate(() => {
                b = Array.prototype.map.call(document.querySelectorAll(".media-body span"), (el) => el.textContent)
                return b.filter(el => el.includes("Furniture")).length
            })
            if(furnitureSize >= index){
                let media = await this.page.evaluate((index) => {
                    let elements = document.querySelectorAll(".media-body span")
                    for (const el of elements) {
                        if(el.textContent.includes(`Furniture ${index}`)){
                            el.click()
                            break;
                        }
                    }
                },index)
                await this.page.waitFor(200)
                if (await this.page.$(infoPanel.removeTaskLink) !== null){
                    this.page.click(infoPanel.removeTaskLink)
                    await this.page.waitForSelector(infoPanel.confirmButton)
                    await this.page.click(infoPanel.confirmButton)
                }
                await this.page.waitForSelector(infoPanel.searchBox)
                await this.page.type(infoPanel.searchBox, `dummy test ${index}`)
                existingFurniture.push(`Furniture${index}`)
                await this.page.waitFor(200)

                // await this.page.keyboard.press('Enter')
                await this.page.waitForSelector(infoPanel.searchResult)
                await this.page.waitFor(selector => document.querySelectorAll(selector).length === 1, {}, infoPanel.searchResult)

                await this.page.click(infoPanel.firstSearchResult)
                await this.page.waitForSelector(infoPanel.assignButton)

                await this.page.click(infoPanel.assignButton)
                await this.page.waitFor(2000)

                await this.page.waitForSelector(infoPanel.removeTaskLink)
                await this.page.click(infoPanel.closeCurrentTab)
                await this.page.waitFor(500)

                await this.page.waitForSelector(infoPanel.infoPanelContainer)
            }
            else {
                break;
            }

        }
        // console.log(`\n\n\n${existingFurniture}\n\n\n`)
        return existingFurniture
    }

    assignStories = async () => {
        let existingStories = []
        let copyFitTimeList = []
        for (let index = 1; index < 5; index++) {
            let storySize = await this.page.evaluate(() => {
                b = Array.prototype.map.call(document.querySelectorAll(".media-body span"), (el) => el.textContent)
                return b.filter(el => el.includes("Stories")).length
            })
            if(storySize >= index){
                let media = await this.page.evaluate((index) => {
                    let elements = document.querySelectorAll(".media-body span")
                    for (const el of elements) {
                        if(el.textContent.includes(`Stories ${index}`)){
                            el.click()
                            break;
                        }
                    }
                },index)
                await this.page.waitFor(200)
                if (await this.page.$(infoPanel.removeTaskLink) !== null){
                    this.page.click(infoPanel.removeTaskLink)
                    await this.page.waitForSelector(infoPanel.confirmButton)
                    await this.page.click(infoPanel.confirmButton)
                }
                await this.page.waitFor(500)

                await this.page.waitForSelector(infoPanel.assigneeButton)
                await this.page.click(infoPanel.assigneeButton)

                await this.page.waitForSelector(infoPanel.assigneeSearchBox)

                let assigneeSearchBox = await this.page.$(infoPanel.assigneeSearchBox)
                await assigneeSearchBox.type("ACM user 3", {delay: 100})
                await this.page.waitFor(300)
                
                await assigneeSearchBox.press('Enter')

                await this.page.waitForSelector(infoPanel.storyDesc)
                await this.page.type(infoPanel.storyDesc,"test")

                await this.page.waitForSelector(infoPanel.assignButton)
                await this.page.click(infoPanel.assignButton)

                await this.page.waitFor(3000)

                await this.page.waitForSelector(infoPanel.searchStoryLink)
                await this.page.click(infoPanel.searchStoryLink)
                await this.page.waitFor(200)

                await this.page.waitForSelector(infoPanel.searchBox)
                await this.page.type(infoPanel.searchBox, `T${index}Heading Dummy Story`, {delay: 10})

                // await this.page.keyboard.press('Enter')
                await this.page.waitForSelector(infoPanel.searchResult)
                await this.page.waitFor(selector => document.querySelectorAll(selector).length === 1, {}, infoPanel.searchResult)

                await this.page.click(infoPanel.firstSearchResult)
                await this.page.waitFor(500)

                try {
                    await this.page.waitForSelector(infoPanel.confirmButton, {timeout: 3000})
                    await this.page.click(infoPanel.confirmButton)
                } catch (error) {
                    // console.log(error)
                }

                try {
                    await this.page.waitForSelector(infoPanel.notificationCloseButton, {timeout: 1000})
                    await this.page.click(infoPanel.notificationCloseButton)
                } catch (error) {
                    // console.log(error)
                }

                await this.page.waitFor(1500)
                

                await this.page.waitForSelector(infoPanel.updateButton)
                await this.page.click(infoPanel.updateButton)
                try {
                    await this.page.waitForSelector(infoPanel.notificationCloseButton, {timeout: 1000})
                    await this.page.click(infoPanel.notificationCloseButton)
                } catch (error) {
                    console.log(error)
                }
                await this.page.waitFor(1000)

                let copyFitFlag = 0
                let copyfitList
                let copyFitTime = 0
                while(copyFitFlag === 0){
                    copyfitList = await this.page.$$(infoPanel.copyFitList)
                    let el = copyfitList[1];
                    let copyFitLeft = await el.$(infoPanel.copyFitLeft)
                    let copyFitPercent = await this.page.evaluate(element => element.innerText, copyFitLeft);
                    let copyFitRight = await el.$(infoPanel.copyFitRight)
                    let copyFitText = await this.page.evaluate(element => element.innerText, copyFitRight);
                    if(copyFitText.includes("Factbox")){
                        break;
                    }
                    copyFitPercent = copyFitPercent.split('%')[0]
                    copyFitPercent = parseInt(copyFitPercent)
                    copyFitFlag = copyFitPercent
                    // console.log(`CopyFit Percent: ${copyFitFlag}`)

                    await this.page.waitFor(10)
                    copyFitTime += 0.01
                }
                // console.log(`CopyFit Time: ${copyFitTime.toFixed(2)}s`)
                copyFitTimeList.push(Number.parseFloat(copyFitTime.toFixed(2)))

                copyfitList = await this.page.$$(infoPanel.copyFitList)
                let validatedStory = {
                    "storyNumber": index,
                    "copyFits": []
                }
                for (let i = copyfitList.length - 1; i >= 0 ; i--) {
                    let el = copyfitList[i];
                    let elClass = await el.getProperty('className')
                    let elClassName = await elClass.jsonValue()
                    if(!elClassName.includes('noborder')){
                        let copyFitLeft = await el.$(infoPanel.copyFitLeft)
                        let copyFitPercent = await this.page.evaluate(element => element.innerText, copyFitLeft);
                        let copyFitRight = await el.$(infoPanel.copyFitRight)
                        let copyFitText = await this.page.evaluate(element => element.innerText, copyFitRight);
                        copyFitPercent = copyFitPercent.split('%')[0]
                        copyFitPercent = parseInt(copyFitPercent)
                        copyFitText = copyFitText.trim()
                        copyFitText = copyFitText.split('*').join('')
                        copyFitText = copyFitText.split(' ').join('').toLowerCase()
        
                        if(copyFitText === 'heading'){
                            copyFitText = 'head'
                        }
                        // console.log(`\nT${index}${copyFitText}: ${copyFitPercent}%\n`)
                        if(!copyFitText.includes("factbox")){
                            // try {
                            //     expect(copyFitPercent).to.be.at.least(1)
                            // } catch (error) {
                            //     console.log(`\nError: expected the percentage of ${copyFitText} in Story ${index} to be bigger than 0%\n`)
                            // }
                            expect(copyFitPercent).to.be.at.least(1, `Error: expected the percentage of ${copyFitText} in Story ${index} to be bigger than 0%`);
                            validatedStory.copyFits.push(`${copyFitText}`)
                        }
                        
                    }
                }

                existingStories.push(validatedStory)
                await this.page.waitFor(500)
                await this.page.waitFor((notificationBox) => {
                    return document.querySelectorAll(notificationBox).length < 1
                }, {}, infoPanel.notificationBox)
                await this.page.click(infoPanel.closeCurrentTab)
                await this.page.waitForSelector(infoPanel.infoPanelContainer)
                await this.page.waitFor(1000)
            }
            else {
                break;
            }

        }
        // existingStories.forEach(el => {
        //     console.log(`\n\n\n${el.storyNumber}\n\n\n`)
        //     el.copyFits.forEach((fit) => console.log(fit, ' '))
        // });
        if (existingStories.length > 0) {
            // console.log(copyFitTimeList)
            const avgCopyFitTime = (copyFitTimeList.reduce((a,b) => a + b, 0) / copyFitTimeList.length)
            console.log(`Average CopyFit Time: ${avgCopyFitTime.toFixed(2)}s`)
            let copyFitTime = {
                "time": avgCopyFitTime.toFixed(2)
            }
            let copyFitTimeJSON = JSON.stringify(copyFitTime)
            let filePath = path.resolve(__dirname, `../results/copyFitTime.json`)
            fs.writeFile(filePath, copyFitTimeJSON, (err) => {if(err) throw err;})
        }

        return existingStories
    }

    crawlPdf = async () => {
        
        await this.page.waitForSelector(infoPanel.downloadPdfButton, {visible: true})
        let previousUrl = await this.page.url()
        await this.page.$eval(infoPanel.downloadPdfButton, el => el.parentElement.click())
        await this.page.waitFor(1000)

        // await this.page.click(infoPanel.downloadPdfButton)
        await this.page.waitFor(previousUrl => window.location.href !== previousUrl, {}, previousUrl)
        // await this.page.waitForNavigation()
        let currentUrl = await this.page.url()
        let response = await crawler(currentUrl)
        let pdfText = response.text.trim()
        pdfText = pdfText.split(' ').join('')
        // console.log(`\n\n\n${pdfText}\n\n\n`)
        return pdfText
    }

    validateElements = async (furnitureList, storyList, pdfData) => {
        for (const furniture of furnitureList) {
            expect(pdfData.toLowerCase().includes(`${furniture}`.toLowerCase()),`Error: expected ${furniture} to be found in PDF`).to.be.true
        }
        for (const story of storyList) {
            for (const copyFit of story.copyFits) {
                expect(pdfData.toLowerCase().includes(`T${story.storyNumber}${copyFit}`.toLowerCase()),`Error: expected T${story.storyNumber}${copyFit} to be found in PDF`).to.be.true
            }
        }
    }
}
